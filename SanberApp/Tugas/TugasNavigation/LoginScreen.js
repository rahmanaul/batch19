import React from 'react';

import { Button, Text, TouchableOpacity, View, StyleSheet } from 'react-native';

const LoginScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Text>Login Screen</Text>
      <Button
        title='Menuju Skill Screen'
        onPress={() => navigation.push('Home')}
      ></Button>
    </View>
  );
};

export default LoginScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    alignItems: 'center',
  },
});
