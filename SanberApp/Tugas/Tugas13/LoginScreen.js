import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput,
  Button,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const input = ['Username', 'Email', 'Password', 'Ulangi Password'];

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require('./Images/logo.png')}
          style={styles.logo}
        ></Image>
        <Text style={styles.pageName}>Register</Text>
        <View style={styles.loginInput}>
          {input.map((prop, key) => {
            if (prop === 'Password' || prop === 'Ulangi Password') {
              return (
                <View style={styles.loginItem}>
                  <Text style={styles.inputTitle} key={key}>
                    {prop}
                  </Text>
                  <TextInput
                    style={styles.textInput}
                    secureTextEntry={true}
                  ></TextInput>
                </View>
              );
            } else {
              return (
                <View style={styles.loginItem}>
                  <Text style={styles.inputTitle} key={key}>
                    {prop}
                  </Text>
                  <TextInput style={styles.textInput}></TextInput>
                </View>
              );
            }
          })}
        </View>
        <TouchableOpacity style={styles.btnDaftar}>
          <Text style={styles.daftarText}>Daftar</Text>
        </TouchableOpacity>
        <Text style={styles.bottomText}>atau</Text>
        <TouchableOpacity style={styles.btnMasuk}>
          <Text style={styles.daftarText}>Masuk ?</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logo: {
    marginTop: 25,
  },
  pageName: {
    marginTop: 75,
    textAlign: 'center',
    fontSize: 24,
    fontFamily: 'Roboto',
    color: '#003366',
  },
  loginInput: {
    marginTop: 40,
    paddingHorizontal: 40,
  },
  inputTitle: {
    color: '#003366',
    fontSize: 16,
  },
  textInput: {
    height: 40,
    borderColor: '#003366',
    borderWidth: 1,
    marginTop: 4,
    marginBottom: 16,
    padding: 5,
  },
  btnDaftar: {
    width: 140,
    height: 40,
    elevation: 8,
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 16,
    backgroundColor: '#003366',
    marginTop: 46,
  },
  daftarText: {
    color: '#FFFFFF',
    fontSize: 24,

    // paddingHorizontal: 49,
  },
  bottomText: {
    textAlign: 'center',
    marginTop: 16,
    fontSize: 24,
    color: '#3EC6FF',
  },
  btnMasuk: {
    width: 140,
    height: 40,
    elevation: 8,
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 16,
    backgroundColor: '#3EC6FF',
    marginTop: 16,
  },
});
