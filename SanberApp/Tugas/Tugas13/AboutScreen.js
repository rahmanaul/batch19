import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput,
  Button,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { FontAwesome } from '@expo/vector-icons';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Tentang Saya</Text>
        <Image
          source={{
            uri:
              'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png',
          }}
          style={styles.foto}
        ></Image>
        <Text style={styles.username}>Aulia Rahman</Text>
        <Text style={styles.role}>React Native Developer</Text>
        <View style={styles.card}>
          <Text style={styles.crdTitle}>Portofolio</Text>
          <View style={styles.prtItem}>
            <View style={styles.prtItem1}>
              <FontAwesome
                name='gitlab'
                size={40}
                color='#3EC6FF'
                style={styles.gitlab}
              />
              <Text
                style={{ color: '#003366', fontSize: 16, fontWeight: 'bold' }}
              >
                @rahmanaul
              </Text>
            </View>
            <View style={styles.prtItem2}>
              <FontAwesome
                name='github'
                size={40}
                color='#3EC6FF'
                style={styles.gitlab}
              />
              <Text
                style={{ color: '#003366', fontSize: 16, fontWeight: 'bold' }}
              >
                @rahmanaul
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.card}>
          <Text style={styles.crdTitle}>Hubungi Saya</Text>
          <View style={styles.contactLogo}>
            <FontAwesome
              name='facebook-square'
              size={40}
              color='#3EC6FF'
              style={styles.gitlab}
            />
            <Text style={styles.contactTxt}>Rahman Aulia</Text>
          </View>
          <View style={styles.contactLogo}>
            <FontAwesome name='instagram' size={40} color='#3EC6FF' />
            <Text style={styles.contactTxt}>@rahmanaull</Text>
          </View>
          <View style={styles.contactLogo}>
            <FontAwesome name='twitter' size={40} color='#3EC6FF' />
            <Text style={styles.contactTxt}>@rahmanaulll</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontFamily: 'Roboto',
    fontSize: 36,
    color: '#003366',
    marginTop: 64,
    alignSelf: 'center',
    flexDirection: 'row',
    fontWeight: 'bold',
  },
  foto: {
    width: 200,
    height: 200,
    borderRadius: 200 / 2,
    alignSelf: 'center',
    marginTop: 12,
  },
  username: {
    fontFamily: 'Roboto',
    fontSize: 24,
    color: '#003366',
    marginTop: 24,
    alignSelf: 'center',
    flexDirection: 'row',
    fontWeight: 'bold',
  },
  role: {
    fontFamily: 'Roboto',
    fontSize: 16,
    color: '#3EC6FF',
    marginTop: 8,
    alignSelf: 'center',
    flexDirection: 'row',
    fontWeight: 'bold',
  },
  card: {
    backgroundColor: '#EFEFEF',
    borderRadius: 16,
    marginHorizontal: 8,
    marginTop: 16,
  },
  crdTitle: {
    marginHorizontal: 8,
    fontFamily: 'Roboto',
    fontSize: 18,
    borderBottomColor: '#003366',
    borderBottomWidth: 1,
    paddingBottom: 8,
    paddingTop: 5,
  },
  prtItem: {
    flexDirection: 'row',
  },
  prtItem1: {
    alignItems: 'center',
    width: 100,
    marginTop: 19.5,
    marginLeft: 45,
    marginBottom: 17,
  },
  prtItem2: {
    alignItems: 'center',
    width: 100,
    marginTop: 19.5,
    marginLeft: 107,
    marginBottom: 17,
  },
  contactLogo: {
    flexDirection: 'row',
    // alignSelf: 'center',
    marginTop: 18.81,
    marginLeft: 89.81,
    paddingRight: 0,
    // borderWidth: 1,
  },
  contactTxt: {
    color: '#003366',
    fontSize: 16,
    fontWeight: 'bold',
    paddingTop: 8,
    marginLeft: 18.81,
  },
});
