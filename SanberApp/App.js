import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

// import YoutubeUI from './Tugas/Tugas12/App';
// import LoginPage from './Tugas/Tugas13/LoginScreen';
// import About from './Tugas/Tugas13/AboutScreen';
// import TodoList from './Tugas/Tugas14/App';
// import Navigation from './Tugas/Tugas15/index';
// import TugasNavigation from './Tugas/TugasNavigation/index';
import Quiz3 from './Tugas/Quiz3/index';

export default function App() {
  // return <YoutubeUI />;
  // return <LoginPage />;
  // return <About />;
  // return <TodoList />;
  // return <Navigation />;
  // return <TugasNavigation />;
  return <Quiz3 />;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
